import os
from torch.utils.data import DataLoader
from torchvision.datasets import ImageFolder
import torchvision.transforms as tt
import torch
import torch.nn as nn
import cv2
from tqdm import tqdm
import torch.nn.functional as F
from torchvision.utils import save_image
from torchvision.utils import make_grid
import matplotlib.pyplot as plt
from torchsummary import summary
import numpy as np
import argparse


from zipfile import ZipFile

# for the nicer images visualization 
# we make inverse transformation for normalization
def denorm(img_tensors, stats):
    return img_tensors * stats[1][0] + stats[0][0]

# functions to plot images
def show_images(images, nmax=64, stats=None):
    fig, ax = plt.subplots(figsize=(8, 8))
    ax.set_xticks([]); ax.set_yticks([])
    ax.imshow(make_grid(denorm(images.detach()[:nmax], stats), nrow=8).permute(1, 2, 0))

def show_batch(dl, nmax=64, stats=None):
    for images, _ in dl:
        show_images(images, nmax, stats)
        break

# Utils functions for GPU usage of neural networks
def get_default_device():
    """Pick GPU if available, else CPU"""
    if torch.cuda.is_available():
        return torch.device('cuda')
    else:
        return torch.device('cpu')
    
def to_device(data, device):
    """Move tensor(s) to chosen device"""
    if isinstance(data, (list,tuple)):
        return [to_device(x, device) for x in data]
    return data.to(device, non_blocking=True)

class DeviceDataLoader():
    """Wrap a dataloader to move data to a device"""
    def __init__(self, dl, device):
        self.dl = dl
        self.device = device
        
    def __iter__(self):
        """Yield a batch of data after moving it to device"""
        for b in self.dl: 
            yield to_device(b, self.device)

    def __len__(self):
        """Number of batches"""
        return len(self.dl)

def calc_out_size_from_conv(input_size, kernel, stride, padding, dilation=1):
    return np.floor((((input_size + 2 * padding - dilation * (kernel - 1) - 1)/ stride) + 1))

def calc_possible_combinations_for_conv(input_size, output_size):
    for kernel in range(2, 5):
        for stride in range(1, 4):
            for padding in range(0, 4):
                if int(calc_out_size_from_conv(input_size, kernel, stride, padding)) == output_size:
                    print(f'possible comb: kernel={kernel} -- stride={stride} -- padding={padding}')

def calc_out_size_from_deconv(inp_size, kernel, stride, padding, dilation=1, out_padding=0):
    return (inp_size - 1) * stride - 2 * padding + dilation * (kernel - 1) + out_padding + 1

def calc_possible_combinations_for_deconv(input_size, output_size):
    for kernel in range(2, 5):
        for stride in range(1, 4):
            for padding in range(0, 4):
                if int(calc_out_size_from_deconv(input_size, kernel, stride, padding)) == output_size:
                    print(f'possible comb: kernel={kernel} -- stride={stride} -- padding={padding}')


def train_discriminator(real_images, opt_d, discriminator, device, batch_size, latent_size, generator):
    # Clear discriminator gradients
    opt_d.zero_grad()

    # Pass real images through discriminator
    real_preds = discriminator(real_images)
    real_targets = torch.ones(real_images.size(0), 1, device=device)
    real_loss = F.binary_cross_entropy(real_preds, real_targets)
    real_score = torch.mean(real_preds).item()
    
    # Generate fake images
    latent = torch.randn(batch_size, latent_size, 1, 1, device=device)
    fake_images = generator(latent)

    # Pass fake images through discriminator
    fake_targets = torch.zeros(fake_images.size(0), 1, device=device)
    fake_preds = discriminator(fake_images)
    fake_loss = F.binary_cross_entropy(fake_preds, fake_targets)
    fake_score = torch.mean(fake_preds).item()

    # Update discriminator weights
    loss = real_loss + fake_loss
    loss.backward()
    opt_d.step()
    return loss.item(), real_score, fake_score

def train_generator(opt_g, generator, device, batch_size, latent_size, discriminator):
    # Clear generator gradients
    opt_g.zero_grad()
    
    # Generate fake images
    latent = torch.randn(batch_size, latent_size, 1, 1, device=device)
    fake_images = generator(latent)
    
    # Try to fool the discriminator
    preds = discriminator(fake_images)
    targets = torch.ones(batch_size, 1, device=device)
    loss = F.binary_cross_entropy(preds, targets)
    
    # Update generator weights
    loss.backward()
    opt_g.step()
    
    return loss.item()


def save_samples(index, latent_tensors, show=True, generator=None, stats=None, sample_dir=None):
    fake_images = generator(latent_tensors)
    random_inds = np.random.randint(0, fake_images.shape[0], 64)
    random_sampled = fake_images[random_inds, : , :, :]
    fake_fname = 'generated-images-{0:0=4d}.png'.format(index)
    denormed = denorm(random_sampled, stats)
    save_image(denormed, os.path.join(sample_dir, fake_fname), nrow=8)
    print('Saving', fake_fname)
    if show:
        fig, ax = plt.subplots(figsize=(8, 8))
        ax.set_xticks([]); ax.set_yticks([])
        ax.imshow(make_grid(denormed.cpu().detach(), nrow=8).permute(1, 2, 0))
        plt.show()

def fit(epochs, lr, start_idx=1, show=False , generator=None, discriminator=None, train_dl=None, device=None, batch_size=64, latent_size=128, fixed_latent=None, stats=None, sample_dir=None):
    torch.cuda.empty_cache()
    
    # Losses & scores
    losses_g = []
    losses_d = []
    real_scores = []
    fake_scores = []
    
    # Create optimizers
    opt_d = torch.optim.Adam(discriminator.parameters(), lr=lr, betas=(0.5, 0.999))
    opt_g = torch.optim.Adam(generator.parameters(), lr=lr, betas=(0.5, 0.999))
    
    for epoch in range(epochs):
        for real_images, _ in tqdm(train_dl):
            # Train discriminator
            loss_d, real_score, fake_score = train_discriminator(real_images, opt_d, discriminator, device, batch_size, latent_size, generator)
            # Train generator
            loss_g = train_generator(opt_g, generator, device, batch_size, latent_size, discriminator)
            
        # Record losses & scores
        losses_g.append(loss_g)
        losses_d.append(loss_d)
        real_scores.append(real_score)
        fake_scores.append(fake_score)
        
        # Log losses & scores (last batch)
        print("Epoch [{}/{}], loss_g: {:.4f}, loss_d: {:.4f}, real_score: {:.4f}, fake_score: {:.4f}".format(
            epoch+1, epochs, loss_g, loss_d, real_score, fake_score))
    
        # Save generated images
        save_samples(epoch+start_idx, fixed_latent, show=show, generator=generator, stats=stats, sample_dir=sample_dir)
    
    return losses_g, losses_d, real_scores, fake_scores

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--latent", "-l", type=int, default=128)
    parser.add_argument("--batch", "-b", type=int, default=256)
    parser.add_argument("--epoch", "-e", type=int, default=100)
    parser.add_argument("--launches", "-ln", type=int, default=1)
    parser.add_argument("--learning_rate", "-lr", type=float, default=0.0001)
    parser.add_argument("--exp_id", "-i", type=int, default=6)
    parser.add_argument("--weights", "-w", type=str, default=None)
    return parser.parse_args()

class Generator6(nn.Module):
    def __init__(self, size_image, latent_dim):
        super(Generator, self).__init__()
        self.size_image = size_image
        self.latent_dim = latent_dim 

        self.init_size = self.size_image // 16   # == 4
        self.l1 = nn.Sequential(nn.Linear(1, self.init_size ** 2))

        self.conv_blocks = nn.Sequential(
            nn.BatchNorm2d(self.latent_dim),
            # init_size == 4
            # out: latent_dim x init_size x init_size
            
            nn.ConvTranspose2d(self.latent_dim, 512, kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.1, inplace=True),
            # out: 512 x 8 x 8

            nn.ConvTranspose2d(512, 256, kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.1, inplace=True),
            # out: 256 x 16 x 16

            nn.ConvTranspose2d(256, 256, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.2, inplace=True),
            # out: 256 x 16 x 16

            nn.ConvTranspose2d(256, 128, kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.1, inplace=True),
            # out: 128 x 32 x 32

            nn.ConvTranspose2d(128, 3, kernel_size=4, stride=2, padding=1, bias=False),
            nn.Tanh()
            # out: 3 x 64 x 64
        )

    def forward(self, z):
        out = self.l1(z)
        # print(f'out shape: {out.shape}')
        out = out.view(batch_size, latent_size, self.init_size, self.init_size)
        # print(f'out shape: {out.shape}')
        img = self.conv_blocks(out)
        # print(f'img shape: {img.shape}')
        return img

class Generator8(nn.Module):
    def __init__(self, size_image, latent_dim):
        super(Generator, self).__init__()
        self.size_image = size_image
        self.latent_dim = latent_dim 

        self.init_size = self.size_image // 4   # == 16
        self.l1 = nn.Sequential(nn.Linear(1, self.init_size ** 2))

        self.conv_blocks = nn.Sequential(
            # nn.BatchNorm2d(self.latent_dim),

            # init_size == 16
            # out: latent_dim x init_size x init_size
            nn.ConvTranspose2d(self.latent_dim, 512, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.2, inplace=True),
            # out: 512 x 16 x 16

            nn.ConvTranspose2d(512, 256, kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.2, inplace=True),
            # out: 256 x 32 x 32

            nn.ConvTranspose2d(256, 256, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.2, inplace=True),
            # out: 256 x 32 x 32

            nn.ConvTranspose2d(256, 128, kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.2, inplace=True),
            # out: 128 x 64 x 64

            nn.ConvTranspose2d(128, 64, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(64),
            nn.LeakyReLU(0.2, inplace=True),
            # out: 128 x 64 x 64

            nn.ConvTranspose2d(64, 3, kernel_size=3, stride=1, padding=1, bias=False),
            nn.Tanh()
            # out: 3 x 64 x 64
        )

    def forward(self, z):
        out = self.l1(z)
        # print(f'out shape: {out.shape}')
        out = out.view(batch_size, latent_size, self.init_size, self.init_size)
        # print(f'out shape: {out.shape}')
        img = self.conv_blocks(out)
        # print(f'img shape: {img.shape}')
        return img


class Generator(nn.Module):
    def __init__(self, size_image, latent_dim):
        super(Generator, self).__init__()
        self.size_image = size_image
        self.latent_dim = latent_dim 

        self.init_size = self.size_image // 8   # == 8
        self.l1 = nn.Sequential(nn.Linear(1, self.init_size ** 2))

        self.conv_blocks = nn.Sequential(
            # nn.BatchNorm2d(self.latent_dim),

            # init_size == 8
            # out: latent_dim x init_size x init_size
            nn.ConvTranspose2d(self.latent_dim, 512, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.2, inplace=True),
            # out: 512 x 8 x 8

            nn.ConvTranspose2d(512, 256, kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.2, inplace=True),
            # out: 256 x 16 x 16

            nn.ConvTranspose2d(256, 256, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.2, inplace=True),
            # out: 256 x 16 x 16

            nn.ConvTranspose2d(256, 128, kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.2, inplace=True),
            # out: 128 x 32 x 32

            nn.ConvTranspose2d(128, 64, kernel_size=4, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(64),
            nn.LeakyReLU(0.2, inplace=True),
            # out: 128 x 64 x 64

            nn.ConvTranspose2d(64, 3, kernel_size=3, stride=1, padding=1, bias=False),
            nn.Tanh()
            # out: 3 x 64 x 64
        )

    def forward(self, z):
        out = self.l1(z)
        # print(f'out shape: {out.shape}')
        out = out.view(batch_size, latent_size, self.init_size, self.init_size)
        # print(f'out shape: {out.shape}')
        img = self.conv_blocks(out)
        # print(f'img shape: {img.shape}')
        return img


discriminator8 = nn.Sequential(
    # in: 3 x 64 x 64

    nn.Conv2d(in_channels = 3, out_channels = 64, kernel_size=3, stride=1, padding=1, bias=False),
    nn.BatchNorm2d(64),
    nn.LeakyReLU(0.1, inplace=True),
    # out: 64 x 64 x 64

    nn.Conv2d(in_channels = 64, out_channels = 128, kernel_size=3, stride=2, padding=1, bias=False),
    nn.BatchNorm2d(128),
    nn.LeakyReLU(0.1, inplace=True),
    # out: 128 x 32 x 32

    nn.Conv2d(128, 256, kernel_size=3, stride=2, padding=1, bias=False),
    nn.BatchNorm2d(256),
    nn.LeakyReLU(0.1, inplace=True),
    # out: 256 x 16 x 16

    nn.Conv2d(256, 512, kernel_size=3, stride=2, padding=1, bias=False),
    # out: 256 x 8 x 8
    nn.BatchNorm2d(512),
    nn.LeakyReLU(0.1, inplace=True),
    nn.MaxPool2d(kernel_size=2, stride=2, padding=0),
    # out: 256 x 4 x 4

    nn.Conv2d(512, 1, kernel_size=3, stride=2, padding=0, bias=False),
    # out: 1 x 1 x 1

    nn.Flatten(),
    nn.Sigmoid())


discriminator = nn.Sequential(
    # in: 3 x 64 x 64

    nn.Conv2d(in_channels = 3, out_channels = 64, kernel_size=3, stride=1, padding=1, bias=False),
    # nn.BatchNorm2d(64),
    nn.LeakyReLU(0.1, inplace=True),
    # out: 64 x 64 x 64

    nn.Conv2d(in_channels = 64, out_channels = 128, kernel_size=4, stride=2, padding=1, bias=False),
    nn.BatchNorm2d(128),
    nn.LeakyReLU(0.1, inplace=True),
    # out: 128 x 32 x 32

    nn.Conv2d(128, 256, kernel_size=3, stride=2, padding=1, bias=False),
    nn.BatchNorm2d(256),
    nn.LeakyReLU(0.1, inplace=True),
    # out: 256 x 16 x 16

    nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1, bias=False),
    # out: 256 x 16 x 16
    nn.BatchNorm2d(256),
    nn.LeakyReLU(0.1, inplace=True),
    nn.MaxPool2d(kernel_size=2, stride=2, padding=0),
    # out: 256 x 8 x 8

    nn.Conv2d(256, 512, kernel_size=4, stride=2, padding=1, bias=False),
    nn.BatchNorm2d(512),
    nn.LeakyReLU(0.1, inplace=True),
    # out: 256 x 4 x 4

    nn.Conv2d(512, 1, kernel_size=3, stride=2, padding=0, bias=False),
    # out: 1 x 1 x 1

    nn.Flatten(),
    nn.Sigmoid())



if __name__ == '__main__':
    args = parse_args()

#     with ZipFile('archive.zip', 'r') as zipObj:
#    # Extract all the contents of zip file in current directory
#         zipObj.extractall()

    DATA_DIR = './cats/'

    # set parameters of the transformed data
    image_size = 64
    batch_size = args.batch
    stats = (0.5, 0.5, 0.5), (0.5, 0.5, 0.5)

    latent_size = args.latent

    # As dataset is stored in the directory, we can create dataset
    # as ImageFolder PyTorch object and set all the transformations here
    train_ds = ImageFolder(DATA_DIR, transform=tt.Compose([
        tt.ToTensor(),
        tt.Normalize(*stats)]))

    # Create PyTorch DataLoader object to produce batches
    train_dl = DataLoader(train_ds, batch_size, shuffle=True, num_workers=2, pin_memory=True)

    device = get_default_device()
    train_dl = DeviceDataLoader(train_dl, device)

    fixed_latent = torch.randn(batch_size, latent_size, 1, 1, device=device)

    generator = Generator(size_image=image_size, latent_dim=latent_size)

    generator = to_device(generator, device)

    discriminator = to_device(discriminator, device)

    PATH_FULL = './weights/'

    if args.weights is not None:

        g_filename = PATH_FULL + 'gen_' + args.weights
        d_filename = PATH_FULL + 'dis_' + args.weights
        generator = torch.load(g_filename)
        discriminator = torch.load(d_filename)


    sample_dir = 'generated'
    weights_dir = 'weights'
    video_dir = 'video'
    os.makedirs(weights_dir, exist_ok=True)
    os.makedirs(sample_dir, exist_ok=True)
    os.makedirs(video_dir, exist_ok=True)

    losses_g_global, losses_d_global, real_scores_global, fake_scores_global = [], [], [], []


    id_number = args.exp_id
    lr = args.learning_rate
    epochs = args.epoch
    launches = args.launches
    step_epoch = epochs // launches

    for epoch in range(launches):
        history = fit(step_epoch, lr, start_idx=step_epoch * epoch, show=False, train_dl=train_dl, device=device, batch_size=batch_size, latent_size=latent_size, fixed_latent=fixed_latent, stats=stats, sample_dir=sample_dir)
        losses_g, losses_d, real_scores, fake_scores = history
        losses_g_global.extend(losses_g)
        losses_d_global.extend(losses_d)
        real_scores_global.extend(real_scores)
        fake_scores_global.extend(fake_scores)
    
        name = f'epochs_{step_epoch * epoch}-{step_epoch * (epoch + 1)}'
        # PATH_FULL = '/content/drive/MyDrive/Sber/SberUni/CV/HW1/'
        g_filename = PATH_FULL + 'gen_' + name + f'__{id_number}.pth'
        d_filename = PATH_FULL + 'dis_' + name + f'__{id_number}.pth'
        
        torch.save(generator, g_filename)
        torch.save(discriminator, d_filename)
        print(f'weights saved for epoch: {step_epoch * (epoch + 1)}')


    vid_fname = f'./video/gans_training_{id_number}.mp4'

    files = [os.path.join(sample_dir, f) for f in os.listdir(sample_dir) if 'generated' in f]
    files.sort()

    out = cv2.VideoWriter(vid_fname,cv2.VideoWriter_fourcc(*'MP4V'), 1, (530,530))
    [out.write(cv2.imread(fname)) for fname in files]
    out.release()

    plt.clf()
    plt.plot(losses_d_global, '-')
    plt.plot(losses_g_global, '-')
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.legend(['Discriminator', 'Generator'])
    plt.title('Losses');
    plt.savefig(f'./video/losses_{id_number}.png')

    plt.clf()
    plt.plot(real_scores_global, '-')
    plt.plot(fake_scores_global, '-')
    plt.xlabel('epoch')
    plt.ylabel('score')
    plt.legend(['Real', 'Fake'])
    plt.title('Scores')
    plt.savefig(f'./video/scores_{id_number}.png');

    print(f'calculatiion is done')