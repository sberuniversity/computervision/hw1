# Homework 1 

# Task 1: Реализовать сверточную нейросеть для классификации котов и собак (0.4 балла)

The corresponding file for this task is [conv_net.ipynb](https://gitlab.com/sberuniversity/computervision/hw1/-/blob/main/conv_net.ipynb).
Weights can be found [here](https://drive.google.com/file/d/1-SfHKjoT6uWk0RzgnPy2oSfG6YQ0KYsU/view?usp=share_link).
In order to load pretrained model, use LOAD_MODEL flag in True mode.
Achieved results:

>Results:
  val accuracy:		82.27 %
Good!
Results:
  test accuracy:		84.11 %
Good!

# Task 2: CVAE (0.3 балла)

The corresponding file for this task is [cvae.ipynb](https://gitlab.com/sberuniversity/computervision/hw1/-/blob/main/cvae.ipynb).
Weights can be found [here](https://drive.google.com/file/d/1ZKFKGN0oWokuKpDLPbl1JTmPw0qPagl6/view?usp=sharing).
In order to load pretrained model, use LOAD_MODEL flag in True mode.


# Task 3: GAN 0.3 балла

The corresponding file for this task is [gan.py](https://gitlab.com/sberuniversity/computervision/hw1/-/blob/main/gan.py).
Different results (weights for generator + discriminator / videos / graphics) can be found [here](https://drive.google.com/drive/folders/1YkoLQAQ5HB5nWy48kYvLb4zEYb-zxvKx?usp=share_link).

Last used weights (and video + graphics) can be found [here](https://drive.google.com/drive/folders/1lCrdE7gBQy1b39Wten-tWC2uDpIIjZ91?usp=share_link)
Download weights to the folder `/weigths`

In order to launch script: 

`python3 gan.py --latent 128 --batch 256 --epoch 100 --launches 5 --learning_rate 0.0001 --epx_id 1 --weights epochs_100-200__93.pth`

where:
>--latent - is the latent size

>--batch - is the size size

>--epoch - is the epoch quantity

>--launches - is the quatity of blocks after each will be saving weights (100 / 5 = 20 epochs and saving)

>--learning_rate - is the learning rate

>--exp_id - is the identical experiment number in order to save the results from different launches

>--weights - is the 2nd part of the file name of weights for the models: generator and discriminator (could be None as well)


# Task 4: Генерируем лица!

The corresponding file for this task is [gan_people.py](https://gitlab.com/sberuniversity/computervision/hw1/-/blob/main/gan_people.py).

Last used weights (and video + graphics) can be found [here](https://drive.google.com/drive/folders/1DZ0J-aeiPoVLtVTRKjmCaL3_xpzRrT--?usp=share_link)
Download weights to the folder `/weigths_face`

In order to launch script: 

`python3 gan_people.py --latent 128 --batch 256 --epoch 100 --launches 5 --learning_rate 0.0001 --epx_id 1 --weights epochs_200-300__3.pth`

where:
>--latent - is the latent size

>--batch - is the size size

>--epoch - is the epoch quantity

>--launches - is the quatity of blocks after each will be saving weights (100 / 5 = 20 epochs and saving)

>--learning_rate - is the learning rate

>--exp_id - is the identical experiment number in order to save the results from different launches

>--weights - is the 2nd part of the file name of weights for the models: generator and discriminator (could be None as well)







