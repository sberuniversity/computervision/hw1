import os
from torch.utils.data import DataLoader
from torchvision.datasets import ImageFolder
import torchvision.transforms as tt
import torch
import torch.nn as nn
import cv2
from tqdm import tqdm
import torch.nn.functional as F
from torchvision.utils import save_image
from torchvision.utils import make_grid
import matplotlib.pyplot as plt
from torchsummary import summary
import numpy as np
import argparse

from gan import *

class Generator(nn.Module):
    def __init__(self, size_image, latent_dim):
        super(Generator, self).__init__()
        self.size_image = size_image
        self.latent_dim = latent_dim 
        # self.in_channels = 512

        self.init_size = self.size_image // 16   # == 8
        self.l1 = nn.Linear(1, self.init_size ** 2)
        self.conv1 = nn.ConvTranspose2d(self.latent_dim, 512, kernel_size=4, stride=2, padding=1, bias=True)
        self.conv2 = nn.ConvTranspose2d(512, 512, kernel_size=4, stride=2, padding=1, bias=True)
        self.conv3 = nn.ConvTranspose2d(512, 256, kernel_size=3, stride=1, padding=1, bias=True)
        self.conv4 = nn.ConvTranspose2d(256, 128, kernel_size=4, stride=2, padding=1, bias=True)
        self.conv5 =  nn.ConvTranspose2d(128, 64, kernel_size=4, stride=2, padding=1, bias=True)
        self.conv6 = nn.ConvTranspose2d(64, 3, kernel_size=3, stride=1, padding=1, bias=True)

        self.bn1 = nn.BatchNorm2d(self.latent_dim)
        self.bn2 = nn.BatchNorm2d(512)
        self.bn3 = nn.BatchNorm2d(512)
        self.bn4 = nn.BatchNorm2d(256)
        self.bn5 = nn.BatchNorm2d(128)
        self.bn6 = nn.BatchNorm2d(64)
        self.tanh = nn.Tanh()
        self.act = nn.ELU(0.1)

        # self.conv_blocks = nn.Sequential(
        #     nn.BatchNorm2d(self.latent_dim),

        #     # init_size == 8
        #     # out: latent_dim x init_size x init_size
        #     nn.ConvTranspose2d(self.latent_dim, 512, kernel_size=4, stride=2, padding=1, bias=False),
        #     nn.BatchNorm2d(512),
        #     nn.LeakyReLU(0.2, inplace=True),
        #     # out: 1024 x 16 x 16

        #     nn.ConvTranspose2d(512, 512, kernel_size=4, stride=2, padding=1, bias=False),
        #     nn.BatchNorm2d(512),
        #     nn.LeakyReLU(0.2, inplace=True),
        #     # out: 512 x 32 x 32

        #     nn.ConvTranspose2d(512, 256, kernel_size=3, stride=1, padding=1, bias=False),
        #     nn.BatchNorm2d(256),
        #     nn.LeakyReLU(0.2, inplace=True),
        #     # out: 256 x 32 x 32

        #     nn.ConvTranspose2d(256, 128, kernel_size=4, stride=2, padding=1, bias=False),
        #     nn.BatchNorm2d(128),
        #     nn.LeakyReLU(0.2, inplace=True),
        #     # out: 128 x 64 x 64

        #     nn.ConvTranspose2d(128, 64, kernel_size=4, stride=2, padding=1, bias=False),
        #     nn.BatchNorm2d(64),
        #     nn.LeakyReLU(0.2, inplace=True),
        #     # out: 64 x 128 x 128

        #     nn.ConvTranspose2d(64, 3, kernel_size=3, stride=1, padding=1, bias=False),
        #     nn.Tanh()
        #     # out: 3 x 128 x 128
        # )

    def forward(self, z):
        out = self.bn1(self.l1(z))
        # print(f'out shape: {out.shape}')
        out = out.view(batch_size, latent_size, self.init_size, self.init_size)
        out = self.act(self.bn2(self.conv1(out)))
        out = self.act(self.bn3(self.conv2(out)))
        out = self.act(self.bn4(self.conv3(out)))
        out = self.act(self.bn5(self.conv4(out)))
        out = self.act(self.bn6(self.conv5(out)))
        img = self.tanh(self.conv6(out))
        # print(f'out shape: {out.shape}')
        # img = self.conv_blocks(out)
        # print(f'img shape: {img.shape}')
        return img

discriminator = nn.Sequential(
    # in: 3 x 128 x 128

    nn.Conv2d(in_channels = 3, out_channels = 128, kernel_size=4, stride=2, padding=1, bias=True),
    # nn.BatchNorm2d(64),
    nn.LeakyReLU(0.1, inplace=True),
    # out: 128 x 64 x 64

    nn.Conv2d(in_channels = 128, out_channels = 256, kernel_size=4, stride=2, padding=1, bias=True),
    nn.BatchNorm2d(256),
    nn.LeakyReLU(0.1, inplace=True),
    # out: 256 x 32 x 32

    nn.Conv2d(256, 512, kernel_size=4, stride=2, padding=1, bias=True),
    nn.BatchNorm2d(512),
    nn.LeakyReLU(0.1, inplace=True),
    # out: 512 x 16 x 16

    nn.Conv2d(512, 1024, kernel_size=4, stride=2, padding=1, bias=True),
    # out: 1024 x 8 x 8
    nn.BatchNorm2d(1024),
    nn.LeakyReLU(0.1, inplace=True),
    # nn.MaxPool2d(kernel_size=4, stride=2, padding=1),
    # out: 1024 x 8 x 8

    nn.Conv2d(1024, 1024, kernel_size=4, stride=2, padding=1, bias=True),
    nn.BatchNorm2d(1024),
    nn.LeakyReLU(0.1, inplace=True),
    # out: 1024 x 4 x 4

    nn.Conv2d(1024, 1, kernel_size=4, stride=1, padding=0, bias=True),
    # out: 1 x 1 x 1

    nn.Flatten(),
    nn.Sigmoid())



if __name__ == '__main__':
    args = parse_args()

    DATA_DIR = './lfw-deepfunneled/'
    

    # set parameters of the transformed data
    batch_size = args.batch
    image_size = 250
    cropped_size = 128
    stats = (0.5, 0.5, 0.5), (0.5, 0.5, 0.5)

    latent_size = args.latent

    # As dataset is stored in the directory, we can create dataset
    # as ImageFolder PyTorch object and set all the transformations here
    train_ds = ImageFolder(DATA_DIR, transform=tt.Compose([
        tt.ToTensor(),
        tt.CenterCrop(cropped_size),
        tt.Normalize(*stats)]
        ))

    # Create PyTorch DataLoader object to produce batches
    train_dl = DataLoader(train_ds, batch_size, shuffle=True, num_workers=2, pin_memory=True)

    device = get_default_device()
    train_dl = DeviceDataLoader(train_dl, device)

    fixed_latent = torch.randn(batch_size, latent_size, 1, 1, device=device)

    generator = Generator(size_image=cropped_size, latent_dim=latent_size)

    generator = to_device(generator, device)

    discriminator = to_device(discriminator, device)

    PATH_FULL = './weights_face/'

    if args.weights is not None:

        g_filename = PATH_FULL + 'gen_' + args.weights
        d_filename = PATH_FULL + 'dis_' + args.weights
        generator = torch.load(g_filename)
        discriminator = torch.load(d_filename)


    sample_dir = 'generated_face'
    weights_dir = 'weights_face'
    video_dir = 'video_face'
    os.makedirs(weights_dir, exist_ok=True)
    os.makedirs(sample_dir, exist_ok=True)
    os.makedirs(video_dir, exist_ok=True)

    losses_g_global, losses_d_global, real_scores_global, fake_scores_global = [], [], [], []


    id_number = args.exp_id
    lr = args.learning_rate
    epochs = args.epoch
    launches = args.launches
    step_epoch = epochs // launches

    for epoch in range(launches):
        history = fit(step_epoch, lr, start_idx=step_epoch * epoch, show=False, generator=generator, discriminator=discriminator, train_dl=train_dl, device=device, batch_size=batch_size, latent_size=latent_size, fixed_latent=fixed_latent, stats=stats, sample_dir=sample_dir)
        losses_g, losses_d, real_scores, fake_scores = history
        losses_g_global.extend(losses_g)
        losses_d_global.extend(losses_d)
        real_scores_global.extend(real_scores)
        fake_scores_global.extend(fake_scores)
    
        name = f'epochs_{step_epoch * epoch}-{step_epoch * (epoch + 1)}'
        # PATH_FULL = '/content/drive/MyDrive/Sber/SberUni/CV/HW1/'
        g_filename = PATH_FULL + 'gen_' + name + f'__{id_number}.pth'
        d_filename = PATH_FULL + 'dis_' + name + f'__{id_number}.pth'
        
        torch.save(generator, g_filename)
        torch.save(discriminator, d_filename)
        print(f'weights saved for epoch: {step_epoch * (epoch + 1)}')


    vid_fname = f'./video_face/gans_training_{id_number}.mp4'

    files = [os.path.join(sample_dir, f) for f in os.listdir(sample_dir) if 'generated' in f]
    files.sort()

    out = cv2.VideoWriter(vid_fname,cv2.VideoWriter_fourcc(*'MP4V'), 1, (530,530))
    [out.write(cv2.imread(fname)) for fname in files]
    out.release()

    plt.clf()
    plt.plot(losses_d_global, '-')
    plt.plot(losses_g_global, '-')
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.legend(['Discriminator', 'Generator'])
    plt.title('Losses');
    plt.savefig(f'./video_face/losses_{id_number}.png')

    plt.clf()
    plt.plot(real_scores_global, '-')
    plt.plot(fake_scores_global, '-')
    plt.xlabel('epoch')
    plt.ylabel('score')
    plt.legend(['Real', 'Fake'])
    plt.title('Scores')
    plt.savefig(f'./video_face/scores_{id_number}.png');

    print(f'calculatiion is done')